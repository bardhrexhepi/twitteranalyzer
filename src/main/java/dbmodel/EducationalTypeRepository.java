package dbmodel;


import org.springframework.data.repository.CrudRepository;

public interface EducationalTypeRepository extends EntityRepository<EducationalTypeEntity>,
        CrudRepository<EducationalTypeEntity, Long> {}