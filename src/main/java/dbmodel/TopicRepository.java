package dbmodel;


import org.springframework.data.repository.CrudRepository;

public interface TopicRepository extends EntityRepository<TopicEntity>,
        CrudRepository<TopicEntity, Long> {}