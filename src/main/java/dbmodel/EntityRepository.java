package dbmodel;



import org.springframework.data.repository.CrudRepository;


public interface EntityRepository<T> {
    public T findFirstByName(String name);
}
