package dbmodel;


import org.springframework.data.repository.CrudRepository;

public interface LanguageRepository extends EntityRepository<LanguageEntity>,
        CrudRepository<LanguageEntity, Long> {}