package dbmodel;

import org.springframework.data.repository.CrudRepository;

public interface AdultContentTypeRepository extends EntityRepository<AdultContentTypeEntity> ,
        CrudRepository<AdultContentTypeEntity, Long>
{}

