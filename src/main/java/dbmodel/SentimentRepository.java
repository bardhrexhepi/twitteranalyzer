package dbmodel;


import org.springframework.data.repository.CrudRepository;

public interface SentimentRepository extends EntityRepository<SentimentEntity>,
        CrudRepository<SentimentEntity, Long> {}