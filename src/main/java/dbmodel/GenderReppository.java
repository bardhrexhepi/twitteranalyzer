package dbmodel;


import org.springframework.data.repository.CrudRepository;

public interface GenderReppository extends EntityRepository<GenderEntity>,
        CrudRepository<GenderEntity, Long> {}