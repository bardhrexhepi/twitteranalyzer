package dbmodel;


import org.springframework.data.repository.CrudRepository;

import java.util.logging.Filter;

public interface FilterRepository extends EntityRepository<FilterEntity> ,
        CrudRepository<FilterEntity, Long> {}
