package dbmodel;


import org.springframework.data.repository.CrudRepository;

public interface ReadabilityRepository extends EntityRepository<ReadabilityEntity>,
        CrudRepository<ReadabilityEntity, Long> {}