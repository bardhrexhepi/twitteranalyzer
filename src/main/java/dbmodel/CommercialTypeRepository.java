package dbmodel;


import org.springframework.data.repository.CrudRepository;

public interface CommercialTypeRepository extends EntityRepository<CommercialTypeEntity>,
        CrudRepository<CommercialTypeEntity, Long> {}