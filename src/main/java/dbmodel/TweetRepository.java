package dbmodel;

import org.springframework.data.repository.CrudRepository;


public interface TweetRepository extends CrudRepository<TweetEntity, Long> {
    public TweetEntity findFirstByTwitterTweetId(Long twitterTweetId);
}