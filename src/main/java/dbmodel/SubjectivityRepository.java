package dbmodel;

import org.springframework.data.repository.CrudRepository;



public interface SubjectivityRepository extends EntityRepository<SubjectivityEntity> ,
        CrudRepository<SubjectivityEntity, Long> {}