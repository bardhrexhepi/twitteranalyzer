package dbmodel;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


import java.util.List;

public interface TwitterUserRepository  extends CrudRepository<TwitterUserEntity, Long> {
    public List<TwitterUserEntity> findByTwitterId(Long twitterId);

}