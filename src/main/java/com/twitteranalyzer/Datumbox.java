package com.twitteranalyzer;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.commons.io.IOUtils;

/**
 * Created by o on 13.07.17.
 */
public class Datumbox {

    private static String BASE_URL = "http://api.datumbox.com/1.0";

    private InputStream postURL(HttpURLConnection connection, URL url,
                               String urlParameters, String request) throws IOException {

        connection.setDoOutput(true);
        connection.setDoInput(true);
        connection.setInstanceFollowRedirects(false);
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        connection.setRequestProperty("charset", "utf-8");
        connection.setRequestProperty("Content-Length", "" + Integer.toString(urlParameters.getBytes().length));
        connection.setUseCaches(false);

        DataOutputStream wr = null;

        try {
            wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
        } finally {
            wr.close();
        }
        InputStream is = connection.getInputStream();
        return is;
    }

    public String req(String request, String text) {
        String result = null;
        try {
            String urlParameters = "api_key=ddc5295ce3de8d7dbc22eb50c27998fc" + "&text=" + text;
            //String request = "http://api.datumbox.com/1.0/SentimentAnalysis.json";
            URL url = new URL(BASE_URL+request);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            InputStream inptStrm = postURL(connection, url, urlParameters, request);
            String jsonTxt = IOUtils.toString(inptStrm);
            result = parse(jsonTxt).replace("\"", "");
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return result;
    }

    public String parse(String jsonLine) {
        JsonElement jelement = new JsonParser().parse(jsonLine);
        JsonObject  jobject = jelement.getAsJsonObject();
        jobject = jobject.getAsJsonObject("output");
        String result = jobject.get("result").toString();
        return result;
    }

    public String sentiment(String text){
        return  req("/SentimentAnalysis.json", text);
    }

    public String twitterSentiment(String text){
        return req("/TwitterSentimentAnalysis.json", text);
    }

    public String subjectivity(String text){
        return req("/SubjectivityAnalysis.json", text);
    }

    public String topic(String text){
        return req("/TopicClassification.json", text);
    }

    public String spam(String text){
        return req("/SpamDetection.json", text);
    }

    public String adultContent(String text){
        return req("/AdultContentDetection.json", text);
    }

    public String readability(String text){
        return req("/ReadabilityAssessment.json ", text);
    }

    public String language(String text){
        return req("/LanguageDetection.json", text);
    }

    public String commercial(String text){
        return req("/CommercialDetection.json", text);
    }

    public String educational(String text){
        return req("/EducationalDetection.json", text);
    }

    public String gender(String text){
        return req("/GenderDetection.json", text);
    }

    public String keyword(String text){
        return req("/KeywordExtraction.json", text);
    }

    public String text(String text){
        return req("/TextExtraction.json", text);
    }
}
