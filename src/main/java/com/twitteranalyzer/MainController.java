package com.twitteranalyzer;

/**
 * Created by o on 12.07.17.
 */
import com.fasterxml.jackson.databind.ObjectMapper;
import dbmodel.*;
import model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Observable;
import java.util.Observer;

import java.text.*;
import java.util.*;


@Controller    // This means that this class is a Controller
@RequestMapping(path="/analyze") // This means URL's start with /analyze (after Application path)
public class MainController{


    @Autowired
    private TwitterUserRepository twitterUserRepository;
    @Autowired
    private CommercialTypeRepository commercialTypeRepository;
    @Autowired
    private AdultContentTypeRepository adultContentTypeRepository;
    @Autowired
    private EducationalTypeRepository educationalTypeRepository;
    @Autowired
    private FilterRepository filterRepository;
    @Autowired
    private LanguageRepository languageRepository;
    @Autowired
    private ReadabilityRepository readabilityRepository;
    @Autowired
    private SentimentRepository sentimentRepository;
    @Autowired
    private SubjectivityRepository subjectivityRepository;
    @Autowired
    private TopicRepository topicRepository;
    @Autowired
    private GenderReppository genderReppository;
    @Autowired
    private TweetRepository tweetRepository;


    private <T, G extends EntityRepository<T> & CrudRepository <T, Long> >  T createEntity(T entity, G repository, String name)
    {
        T result = repository.findFirstByName(name);
        if(result == null){
            repository.save(entity);
            result = repository.findFirstByName(name);
        }
        return result;
    }

    @PostMapping(value = "/tweet")
    private String process(@RequestBody String content){
        String returnValue;
        Datumbox datumbox = new Datumbox();
        ObjectMapper mapper = new ObjectMapper();
        try {

            Tweet tweet = mapper.readValue(content, Tweet.class);


            TwitterUserEntity twitterUserEntity = new TwitterUserEntity(
                    tweet.getUser().getName(),
                    tweet.getUser().getScreenName(),
                    tweet.getUser().getId(),
                    tweet.getUser().getProfileImageUrl()
            );

            List<TwitterUserEntity> twitterUserEntityList = twitterUserRepository.findByTwitterId(twitterUserEntity.getTwitterId());
            if(twitterUserEntityList != null){
                if(twitterUserEntityList.isEmpty()){
                    twitterUserRepository.save(twitterUserEntity);
                    twitterUserEntityList = twitterUserRepository.findByTwitterId(twitterUserEntity.getTwitterId());
                }
            }
            twitterUserEntity = twitterUserEntityList.iterator().next();



            TweetEntity tweetEntity = new TweetEntity();

            String stringDate = tweet.getCreatedAt();
            SimpleDateFormat df = new SimpleDateFormat("EEE MMM dd hh:mm:ss yyyy");
            Date date = df.parse(stringDate.substring(0, stringDate.indexOf('+')) + stringDate.substring(stringDate.length()-5));
            tweetEntity.setDate(date);

            String tweetText = tweet.getText();
            tweetEntity.setContent(tweetText);

            String commercial = datumbox.commercial(tweetText);
            tweetEntity.setCommercialType(createEntity(new CommercialTypeEntity(commercial), commercialTypeRepository, commercial));

            String adultContent = datumbox.adultContent(tweetText);
            tweetEntity.setContentType(createEntity(new AdultContentTypeEntity(adultContent), adultContentTypeRepository, adultContent));

            String educational = datumbox.educational(tweetText);
            tweetEntity.setEducationalType(createEntity(new EducationalTypeEntity(educational), educationalTypeRepository, educational));

            String gender = datumbox.gender(tweetText);
            tweetEntity.setGender(createEntity(new GenderEntity(gender), genderReppository, gender));

            String language = datumbox.language(tweetText);
            tweetEntity.setLanguage(createEntity(new LanguageEntity(language), languageRepository, language));

            String readability = datumbox.readability(tweetText);
            tweetEntity.setReadability(createEntity(new ReadabilityEntity(readability), readabilityRepository, readability));

            String sentiment = datumbox.sentiment(tweetText);
            tweetEntity.setSentiment(createEntity(new SentimentEntity(sentiment), sentimentRepository, sentiment));

            String topic = datumbox.topic(tweetText);
            tweetEntity.setTopic(createEntity(new TopicEntity(topic), topicRepository, topic));

            String subjectivity = datumbox.subjectivity(tweetText);
            tweetEntity.setSubjectivity(createEntity(new SubjectivityEntity(subjectivity), subjectivityRepository, subjectivity));

            tweetEntity.setTwitterTweetId(tweet.getId());
            tweetEntity.setUserEntity(twitterUserEntity);
            tweetRepository.save(tweetEntity);


            returnValue = tweetText + " " + commercial + " " + adultContent + " " + educational;

        }
        catch (Exception e){
            e.printStackTrace();
            returnValue = "Failed." + e.getMessage();
        }

        return returnValue;

    }



}
